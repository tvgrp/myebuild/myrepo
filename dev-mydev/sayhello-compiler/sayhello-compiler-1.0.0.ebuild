# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

DESCRIPTION="This package only for portage education."
HOMEPAGE="https://gitlab.com/tvgrp/myebuild/sayhello"
SRC_URI="https://gitlab.com/tvgrp/myebuild/sayhello/-/archive/1.0.0/sayhello-1.0.0.tar.gz"
RESTRICT="mirror"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64"
IUSE="+compiler_use"

RDEPEND=""
DEPEND="${RDEPEND}"
BDEPEND=""


src_unpack() {
	unpack  ${A}
	mv ${WORKDIR}/sayhello-1.0.0 ${S}
}

src_compile() {
	einfo "This is info."
	elog "This is log."

	emake execname=${PN}
}

src_install() {
	dobin ${PN}
}
